﻿// File:    CCustomer.java
// Author:  甘孝俭
// email:   ganxiaojian@hotmail.com 
// QQ:      154986287
// http://www.8088net.com
// 协议声明：本软件为开源系统，遵循国际开源组织协议。任何单位或个人可以使用或修改本软件源码，
//          可以用于作为非商业或商业用途，但由于使用本源码所引起的一切后果与作者无关。
//          未经作者许可，禁止任何企业或个人直接出售本源码或者把本软件作为独立的功能进行销售活动，
//          作者将保留追究责任的权利。
// Created: 2015/12/6 10:23:34
// Purpose: Definition of Class CCustomer

package com.ErpCoreModel.Store;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.Framework.CValue;

public class CCustomer extends CBaseObject
{

    public CCustomer()
    {
        TbCode = "KH_Customer";
        ClassName = "com.ErpCoreModel.Store.CCustomer";

    }

    
        public String getName()
        {
            if (m_arrNewVal.containsKey("name"))
                return m_arrNewVal.get("name").StrVal;
            else
                return "";
        }
        public void setName(String value)
        {
            if (m_arrNewVal.containsKey("name"))
                m_arrNewVal.get("name").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("name", val);
            } 
       }
        public String getPwd()
        {
            if (m_arrNewVal.containsKey("pwd"))
                return m_arrNewVal.get("pwd").StrVal;
            else
                return "";
        }
        public void setPwd(String value)
        {
            if (m_arrNewVal.containsKey("pwd"))
                m_arrNewVal.get("pwd").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("pwd", val);
            } 
       }
        public String getTName()
        {
            if (m_arrNewVal.containsKey("tname"))
                return m_arrNewVal.get("tname").StrVal;
            else
                return "";
        }
        public void setTName(String value)
        {
            if (m_arrNewVal.containsKey("tname"))
                m_arrNewVal.get("tname").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("tname", val);
            } 
       }
        public Boolean getSex()
        {
            if (m_arrNewVal.containsKey("sex"))
                return m_arrNewVal.get("sex").BoolVal;
            else
                return false;
        }
        public void setSex(Boolean value)
        {
            if (m_arrNewVal.containsKey("sex"))
                m_arrNewVal.get("sex").BoolVal = value;
            else
            {
                CValue val = new CValue();
                val.BoolVal = value;
                m_arrNewVal.put("sex", val);
            }
        }
        public Date getBirthday()
        {
    	    if (m_arrNewVal.containsKey("birthday"))
                return m_arrNewVal.get("birthday").DatetimeVal;
            else
                return new Date(System.currentTimeMillis());
        }
        void setBirthday(Date value)
        {      
            if (m_arrNewVal.containsKey("birthday"))
                m_arrNewVal.get("birthday").DatetimeVal=value;
            else
            {
                CValue val = new CValue();
                val.DatetimeVal= value;
                m_arrNewVal.put("birthday", val);
            }
        }
        public String getAddr()
        {
            if (m_arrNewVal.containsKey("addr"))
                return m_arrNewVal.get("addr").StrVal;
            else
                return "";
        }
        public void setAddr(String value)
        {
            if (m_arrNewVal.containsKey("addr"))
                m_arrNewVal.get("addr").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("addr", val);
            } 
       }
        public String getZipcode()
        {
            if (m_arrNewVal.containsKey("zipcode"))
                return m_arrNewVal.get("zipcode").StrVal;
            else
                return "";
        }
        public void setZipcode(String value)
        {
            if (m_arrNewVal.containsKey("zipcode"))
                m_arrNewVal.get("zipcode").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("zipcode", val);
            } 
       }
        public String getCompany()
        {
            if (m_arrNewVal.containsKey("company"))
                return m_arrNewVal.get("company").StrVal;
            else
                return "";
        }
        public void setCompany(String value)
        {
            if (m_arrNewVal.containsKey("company"))
                m_arrNewVal.get("company").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("company", val);
            } 
       }
        public String getTel()
        {
            if (m_arrNewVal.containsKey("tel"))
                return m_arrNewVal.get("tel").StrVal;
            else
                return "";
        }
        public void setTel(String value)
        {
            if (m_arrNewVal.containsKey("tel"))
                m_arrNewVal.get("tel").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("tel", val);
            } 
       }
        public String getPhone()
        {
            if (m_arrNewVal.containsKey("phone"))
                return m_arrNewVal.get("phone").StrVal;
            else
                return "";
        }
        public void setPhone(String value)
        {
            if (m_arrNewVal.containsKey("phone"))
                m_arrNewVal.get("phone").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("phone", val);
            } 
       }
        public String getE_mail()
        {
            if (m_arrNewVal.containsKey("e_mail"))
                return m_arrNewVal.get("e_mail").StrVal;
            else
                return "";
        }
        public void setE_mail(String value)
        {
            if (m_arrNewVal.containsKey("e_mail"))
                m_arrNewVal.get("e_mail").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("e_mail", val);
            } 
       }
        public String getQQ()
        {
            if (m_arrNewVal.containsKey("qq"))
                return m_arrNewVal.get("qq").StrVal;
            else
                return "";
        }
        public void setQQ(String value)
        {
            if (m_arrNewVal.containsKey("qq"))
                m_arrNewVal.get("qq").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("qq", val);
            } 
       }
        public String getWW()
        {
            if (m_arrNewVal.containsKey("ww"))
                return m_arrNewVal.get("ww").StrVal;
            else
                return "";
        }
        public void setWW(String value)
        {
            if (m_arrNewVal.containsKey("ww"))
                m_arrNewVal.get("ww").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("ww", val);
            } 
       }
        public String getRemarks()
        {
            if (m_arrNewVal.containsKey("remarks"))
                return m_arrNewVal.get("remarks").StrVal;
            else
                return "";
        }
        public void setRemarks(String value)
        {
            if (m_arrNewVal.containsKey("remarks"))
                m_arrNewVal.get("remarks").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("remarks", val);
            } 
       }
}
