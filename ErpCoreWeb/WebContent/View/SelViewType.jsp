<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.ErpCoreWeb.Common.Global" %>
<%@ page import="com.ErpCoreModel.Framework.CTable" %>
<%@ page import="com.ErpCoreModel.Framework.Util" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObject" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObjectMgr" %>
<%@ page import="com.ErpCoreModel.Framework.CColumn" %>
<%@ page import="com.ErpCoreModel.Base.CUser" %>
<%@ page import="com.ErpCoreModel.UI.CView" %>
<%@ page import="com.ErpCoreModel.UI.enumViewType" %>
    
<%
if (request.getSession().getAttribute("User") == null)
{
    response.sendRedirect("../Login.jsp");
    response.getWriter().close();
}

//编辑视图
String id = request.getParameter("id");
if (!Global.IsNullParameter(id))
{
	CView m_View = (CView)Global.GetCtx(this.getServletContext()).getViewMgr().Find(Util.GetUUID(id));
    if(m_View.getVType()== enumViewType.Single)
    	response.sendRedirect("SingleViewInfo1.jsp?id="+request.getParameter("id")+"&catalog_id="+request.getParameter("catalog_id"));
    else if(m_View.getVType()== enumViewType.MasterDetail)
    	response.sendRedirect("MasterDetailViewInfo1.jsp?id="+request.getParameter("id")+"&catalog_id="+request.getParameter("catalog_id"));
    else
    	response.sendRedirect("MultMasterDetailViewInfo1.jsp?id="+request.getParameter("id")+"&catalog_id="+request.getParameter("catalog_id"));
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" /> 
    <link href="../lib/ligerUI/skins/Gray/css/all.css" rel="stylesheet" type="text/css" /> 
    <script src="../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
     <script src="../lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerButton.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script> 
    <script src="../lib/ligerUI/js/plugins/ligerTip.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.validate.min.js" type="text/javascript"></script> 
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    
    <script type="text/javascript">

	    function btOk_Click() {
	    	if ($("#rdSingle").attr("checked"))
	    		document.location.href= "SingleViewInfo1.jsp?id=<%=request.getParameter("id")%>&catalog_id=<%=request.getParameter("catalog_id")%>"  ;
	        else if ($("#rdMasterDetail").attr("checked"))
	        	document.location.href="MasterDetailViewInfo1.jsp?id=<%=request.getParameter("id")%>&catalog_id=<%=request.getParameter("catalog_id")%>"  ;
	        else
	        	document.location.href="MultMasterDetailViewInfo1.jsp?id=<%=request.getParameter("id")%>&catalog_id=<%=request.getParameter("catalog_id")%>"  ;
	    }
        function btCancel_onclick() {
            parent.$.ligerDialog.close();
        }

    </script>
    <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;}
    </style>
</head>
<body style="padding:10px">
    <form id="form1" >
    <div >
        <p>选择视图类型：</p>
        <br />
        
        <table cellpadding="0" cellspacing="0" class="l-table-edit" style="height:50px">
            <tr>
                <td align="left">
                    <input type="radio" id="rdSingle" name="viewtype" checked="checked"  />单表视图
                    </td>
            </tr>
            <tr>
                <td align="left">
                    <input type="radio" id="rdMasterDetail" name="viewtype"   />主从表视图
                    </td>
            </tr>
            <!-- 
            <tr>
                <td align="left">
                    <input type="radio" id="rdMultMasterDetail" name="viewtype"   />多主从表视图
                    </td>
            </tr>
             -->
        </table>
        <br />
        <p>
            <input type="button" id="btOk" name="btOk" style="width:60px"  value="确定" onclick="return btOk_Click()" />
            &nbsp;&nbsp;&nbsp;&nbsp;
            <input type="button" id="btCancel" name="btCancel" style="width:60px"  value="取消" onclick="return btCancel_onclick()" />
            </p>
    </div>
    </form>
</body>
</html>