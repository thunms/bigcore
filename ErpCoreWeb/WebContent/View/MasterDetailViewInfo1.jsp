<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.ErpCoreWeb.Common.Global" %>
<%@ page import="com.ErpCoreModel.Framework.Util" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObject" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObjectMgr" %>
<%@ page import="com.ErpCoreModel.Framework.CTable" %>
<%@ page import="com.ErpCoreModel.UI.CView" %>
<%@ page import="com.ErpCoreModel.UI.CViewCatalog" %>
<%@ page import="com.ErpCoreModel.UI.CViewDetail" %>
<%@ page import="com.ErpCoreModel.UI.enumViewType" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.UUID" %>
<%@ page import="java.util.HashMap" %>
    
<%
if (request.getSession().getAttribute("User") == null)
{
    response.sendRedirect("../Login.jsp");
    return ;
}
CView m_View=null;
CViewDetail m_ViewDetail =null;
CTable m_Table =null;
CTable m_DetailTable =null;

String id = request.getParameter("id");
boolean m_bIsNew=false;
if (!Global.IsNullParameter(id))
{
    m_View = (CView)Global.GetCtx(this.getServletContext()).getViewMgr().Find(Util.GetUUID(id));
}
else
{
    m_bIsNew = true;
    if (request.getSession().getAttribute("NewMasterDetailView") == null)
    {
        m_View = new CView();
        m_View.Ctx = Global.GetCtx(this.getServletContext());
        m_View.setVType( enumViewType.MasterDetail);
        CViewDetail ViewDetail = new CViewDetail();
        ViewDetail.Ctx = m_View.Ctx;
        ViewDetail.setUI_View_id ( m_View.getId());
        m_View.getViewDetailMgr().AddNew(ViewDetail);

        Map<UUID, CView> sortObj = new HashMap<UUID, CView>();
        sortObj.put(m_View.getId(), m_View);
        request.getSession().setAttribute("NewMasterDetailView", sortObj);
        
    }
    else
    {
    	Map<UUID, CView> sortObj = (Map<UUID, CView>)request.getSession().getAttribute("NewMasterDetailView");
        m_View =(CView) sortObj.values().toArray()[0];
    }
}
m_Table = (CTable)Global.GetCtx(this.getServletContext()).getTableMgr().Find(m_View.getFW_Table_id());
m_ViewDetail = (CViewDetail)m_View.getViewDetailMgr().GetFirstObj();
if (m_ViewDetail != null)
	m_DetailTable = (CTable)Global.GetCtx(this.getServletContext()).getTableMgr().Find(m_ViewDetail.getFW_Table_id());
%>
<%!
String LoadCatalog()
{
    String sJson="";
    List<CBaseObject> lstObj = Global.GetCtx(this.getServletContext()).getViewCatalogMgr().GetList();
    for (CBaseObject obj : lstObj)
    {
        CViewCatalog catalog = (CViewCatalog)obj;
        sJson += String.format("{ text: '%s', id: '%s' },",catalog.getName(), catalog.getId().toString());
    }
	if (sJson.endsWith(","))
		sJson = sJson.substring(0, sJson.length() - 1);
	sJson = "[" + sJson + "]";
	
	return sJson;
}
String LoadTable()
{
	String sJson="";
    List<CBaseObject> lstObj = Global.GetCtx(this.getServletContext()).getTableMgr().GetList();
    for (CBaseObject obj : lstObj)
    {
        CTable table = (CTable)obj;
        sJson += String.format("{ text: '%s', id: '%s' },",table.getName(), table.getId().toString());
    }
	if (sJson.endsWith(","))
		sJson = sJson.substring(0, sJson.length() - 1);
	sJson = "[" + sJson + "]";
	
	return sJson;
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" /> 
    <link href="../lib/ligerUI/skins/Gray/css/all.css" rel="stylesheet" type="text/css" /> 
    <script src="../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
     <script src="../lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerButton.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script> 
    <script src="../lib/ligerUI/js/plugins/ligerTip.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.validate.min.js" type="text/javascript"></script> 
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    
    <script type="text/javascript">
  //下拉框
  var CatalogData=<%=LoadCatalog()%>
  var TableData=<%=LoadTable()%>
  var curPrimaryKey='<%=m_ViewDetail!=null?m_ViewDetail.getPrimaryKey():""%>';
  var curForeignKey='<%=m_ViewDetail!=null?m_ViewDetail.getForeignKey():""%>';
  
    $(function() {
   	 	<%if(m_View!=null){%>
    	$("#txtName").val("<%=m_View.getName()%>");
    	<%}%>
    	
    	$("#cbCatalog").ligerComboBox({
        	data:CatalogData,
          	initValue:'<%=request.getParameter("catalog_id")%>',
          	valueFieldID: 'cbCatalogId'
        });
    	
    	var MasterTable='<%=m_View!=null?m_View.getFW_Table_id().toString():""%>';
    	$("#cbMasterTable").ligerComboBox({
        	data:TableData,
          	initValue:MasterTable,
          	valueFieldID: 'cbMasterTableId',
        	onSelected: function (newvalue)
            {
        		LoadPrimaryKey(newvalue);
            }
        });
    	var DetailTable='<%=m_ViewDetail!=null?m_ViewDetail.getFW_Table_id().toString():""%>';
    	$("#cbDetailTable").ligerComboBox({
        	data:TableData,
          	initValue:DetailTable,
          	valueFieldID: 'cbDetailTableId',
        	onSelected: function (newvalue)
            {
        		LoadForeignKey(newvalue);
            }
        });

        
    	var url="";
   	 	<%if(m_View!=null){%>
   	 		url="MasterDetailViewInfo1.do?Action=LoadColumn&id=<%=request.getParameter("id") %>&catalog_id=<%=request.getParameter("catalog_id") %>&table_id=<%=m_View.getFW_Table_id()%>";
   	 	<%}%>
    	$("#cbPrimaryKey").ligerComboBox({ url: url, isMultiSelect: false,
          	initValue:curPrimaryKey,
          	valueFieldID: 'cbPrimaryKeyId' });
   	 	
   	 	var url2="";
   	 	<%if(m_View!=null){%>
   	 		url2="MasterDetailViewInfo1.do?Action=LoadColumn&id=<%=request.getParameter("id") %>&catalog_id=<%=request.getParameter("catalog_id") %>&table_id=<%=m_ViewDetail.getFW_Table_id()%>";
   	 	<%}%>
   	 	$("#cbForeignKey").ligerComboBox({ url: url2, isMultiSelect: false,
          	initValue:curForeignKey,
          	valueFieldID: 'cbForeignKeyId' });

        
    });
    
    function LoadPrimaryKey(table_id)
    {
    	$.post(
            'MasterDetailViewInfo1.do',
            {
                id: '<%=request.getParameter("id") %>',
                catalog_id: '<%=request.getParameter("catalog_id") %>',
                table_id:table_id,
                Action: 'LoadColumn'
            },
             function (data) {
                 if (data == "" || data == null) {
                     return false;
                 }
                 else {
                	 $("#cbPrimaryKey").ligerGetComboBoxManager().setData(data);
                     return true;
                 }
             },
             'json');
    }
    function LoadForeignKey(table_id)
    {
    	$.post(
            'MasterDetailViewInfo1.do',
            {
                id: '<%=request.getParameter("id") %>',
                catalog_id: '<%=request.getParameter("catalog_id") %>',
                table_id:table_id,
                Action: 'LoadColumn'
            },
             function (data) {
                 if (data == "" || data == null) {
                     return false;
                 }
                 else {
                	 $("#cbForeignKey").ligerGetComboBoxManager().setData(data);
                     return true;
                 }
             },
             'json');
    }
    
   

    function onNext() {
        $.post(
            'MasterDetailViewInfo1.do',
            {
                id: '<%=request.getParameter("id") %>',
                catalog_id: $("#cbCatalogId").val(),
                txtName:$("#txtName").val(),
                cbMasterTable:$("#cbMasterTableId").val(),
                cbDetailTable:$("#cbDetailTableId").val(),
                cbPrimaryKey:$("#cbPrimaryKeyId").val(),
                cbForeignKey:$("#cbForeignKeyId").val(),
                Action: 'Next'
            },
             function (data) {
                 if (data == "" || data == null) {
                	 var url="MasterDetailViewInfo2.jsp?id=<%=request.getParameter("id")%>&catalog_id=<%=request.getParameter("catalog_id")%>";
                	 document.location.href=url;
                     return true;
                 }
                 else {
                     return false;
                 }
             },
             'text');
    }    
    function onCancel() {
        $.post(
            'MasterDetailViewInfo1.do',
            {
                id: '<%=request.getParameter("id") %>',
                catalog_id: '<%=request.getParameter("catalog_id") %>',
                Action: 'Cancel'
            },
             function (data) {
                 if (data == "" || data == null) {
                	 parent.$.ligerDialog.close();
                	 return true;
                 }
                 else {
                	$.ligerDialog.warn(data);
                     return false;
                 }
             },
             'text');
    } 
    </script>
    <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;}
    </style>
</head>
<body style="padding:10px">
    <form id="form1" action="MasterDetailViewInfo1.do">
    <div >
        <p>基本定义：</p>
        <br />
        
        <table cellpadding="0" cellspacing="0" class="l-table-edit" style="height:50px">
            <tr>
                <td align="left">
                    &nbsp;名称：</td>
                <td align="left">
                    &nbsp;<input id="txtName" name="txtName" /></td>
            </tr>
            <tr>
                <td align="left">
                    &nbsp;目录</td>
                <td align="left">
                    &nbsp;
                    <input id="cbCatalog" name="cbCatalog" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    &nbsp;主表：</td>
                <td align="left">
                    &nbsp;
                    <input id="cbMasterTable" name="cbMasterTable" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    &nbsp;从表：</td>
                <td align="left">
                    &nbsp;
                    <input id="cbDetailTable" name="cbDetailTable" />
                    
                </td>
            </tr>
            <tr>
                <td align="left">
                    &nbsp;外键关联：</td>
                <td align="left">
                    &nbsp;
                    <input id="cbPrimaryKey" name="cbPrimaryKey" />
                    ——
                    <input id="cbForeignKey" name="cbForeignKey" />
                    
                </td>
            </tr>
            
        </table>
        <br />
        <p>
            <input type="button" id="btNext" style="width:60px" value="下一步" onclick="onNext();" />
            &nbsp;&nbsp;&nbsp;&nbsp;
            <input type="button" id="btCancel" style="width:60px" value="取消" onclick="onCancel();" />
            </p>
    </div>
    </form>
</body>
</html>