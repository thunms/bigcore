package com.ErpCoreWeb.Window;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.dispatcher.multipart.MultiPartRequestWrapper;

import com.ErpCoreModel.Base.CUser;
import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.CBaseObjectMgr;
import com.ErpCoreModel.Framework.CColumn;
import com.ErpCoreModel.Framework.CTable;
import com.ErpCoreModel.Framework.ColumnType;
import com.ErpCoreModel.Framework.DatabaseType;
import com.ErpCoreModel.Framework.DbParameter;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreWeb.Common.Global;
import com.mongodb.BasicDBObject;

/**
 * Servlet implementation class TableWindow
 */
@WebServlet("/TableWindow")
public class TableWindow extends HttpServlet {
	private static final long serialVersionUID = 1L;

	HttpServletRequest request;
	HttpServletResponse response;

	public CUser m_User = null;
    public CTable m_Table = null;
    public UUID m_guidParentId = Util.GetEmptyUUID();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TableWindow() {
        super();
        // TODO Auto-generated constructor stub
    }

    //初始化变量
    void initData()
    {
        if (request.getSession().getAttribute("User") == null)
        {
            try {
				response.sendRedirect("../Login.jsp");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return ;
        }
        m_User=(CUser)request.getSession().getAttribute("User");
        if (!m_User.IsRole("管理员"))
        {
        	try {
				response.getWriter().print("没有管理员权限！");
	        	response.getWriter().close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	return ;
        }
        
        String tid = request.getParameter("tid");
        if (Global.IsNullParameter(tid))
        {
            try {
				response.getWriter().close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        m_Table = (CTable)Global.GetCtx(this.getServletContext()).getTableMgr().Find(Util.GetUUID(tid));
        
        String ParentId = request.getParameter("ParentId");
        if (!Global.IsNullParameter(ParentId))
            m_guidParentId = Util.GetUUID(ParentId);

    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}
	void doProc(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		this.request=request;
		this.response=response;

		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		//初始化变量
	    initData();
	    //
        String Action = request.getParameter("Action");
        if (Action == null) Action = "";

        if (Action.equalsIgnoreCase("GetData"))
        {
        	GetData();
            return ;
        }
        else if (Action.equalsIgnoreCase("Delete"))
        {
        	Delete();
            return ;
        }
	}

    void GetData()
    {
    	int page = Integer.valueOf(request.getParameter("page"));
		int pageSize = Integer.valueOf(request.getParameter("pagesize"));
        long totalCount = 0;

        CBaseObjectMgr BaseObjectMgr = Global.GetCtx(this.getServletContext()).FindBaseObjectMgrCache(m_Table.getCode(), m_guidParentId);
        if (BaseObjectMgr == null)
        {
            BaseObjectMgr = new CBaseObjectMgr();
            BaseObjectMgr.TbCode = m_Table.getCode();
            BaseObjectMgr.Ctx = Global.GetCtx(this.getServletContext());
        }

        String sData = "";
        List<CBaseObject> lstObj = new ArrayList<CBaseObject>();
        if (!m_Table.getIsLowChange())
        {
            totalCount = BaseObjectMgr.GetCount();
            int nSkip = (page - 1) * pageSize; // 开始记录数  
            lstObj = BaseObjectMgr.GetList(null, null, nSkip, pageSize);
        }
        else
        {
            lstObj = BaseObjectMgr.GetList();
            totalCount = lstObj.size();
        }

        long totalPage = totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1; // 计算总页数

        int index = (page - 1) * pageSize; // 开始记录数  
        if (!m_Table.getIsLowChange())
            index = 0;
        for (int i = index; i < pageSize + index ; i++)
        {
            if (i >= lstObj.size())
                break;
            CBaseObject obj = (CBaseObject)lstObj.get(i);


            String sRow="";
            for (CBaseObject objC : m_Table.getColumnMgr().GetList())
            {
                CColumn col = (CColumn)objC;
                if (col.getColType() == ColumnType.object_type)
                {
                	String sVal = "";
                    if (obj.GetColValue(col) != null)
                        sVal = "long byte";
                    sRow += String.format("\"%s\":\"%s\",", col.getCode(), sVal);
                }
                else if (col.getColType() == ColumnType.ref_type)
                {
                    CTable RefTable = (CTable)Global.GetCtx(this.getServletContext()).getTableMgr().Find(col.getRefTable());
                    if (RefTable == null) continue;
                    CColumn RefCol = (CColumn)RefTable.getColumnMgr().Find(col.getRefCol());
                    CColumn RefShowCol = (CColumn)RefTable.getColumnMgr().Find(col.getRefShowCol());
                    Object objVal = obj.GetColValue(col);


                    String sVal = "";
                    UUID guidParentId = Util.GetEmptyUUID();
                    if (BaseObjectMgr.m_Parent != null && BaseObjectMgr.m_Parent.getId().equals((UUID)objVal))
                    {
                        Object objVal2 = BaseObjectMgr.m_Parent.GetColValue(RefShowCol);
                        if (objVal2 != null)
                            sVal = objVal2.toString();
                    }
                    else
                    {
                        CBaseObjectMgr objRefMgr = Global.GetCtx(this.getServletContext()).FindBaseObjectMgrCache(RefTable.getCode(), guidParentId);
                        if (objRefMgr != null)
                        {
                            CBaseObject objCache = objRefMgr.FindByValue(RefCol, objVal);
                            if (objCache != null)
                            {
                                Object objVal2 = objCache.GetColValue(RefShowCol);
                                if (objVal2 != null)
                                    sVal = objVal2.toString();
                            }
                        }
                        else
                        {
                            objRefMgr = new CBaseObjectMgr();
                            objRefMgr.TbCode = RefTable.getCode();
                            objRefMgr.Ctx = Global.GetCtx(this.getServletContext());

                            if (objRefMgr.Ctx.getMainDB().m_DbType == DatabaseType.MongoDb)
                            {
                            	BasicDBObject query = new BasicDBObject();  
                                query.put(RefCol.getBsonCode(), obj.GetColValue(col));
                                List<CBaseObject> lstObj2 = objRefMgr.GetList(query, null, 0, 0);
                                if (lstObj2.size() > 0)
                                {
                                    CBaseObject obj2 = lstObj2.get(0);
                                    Object objVal2 = obj2.GetColValue(RefShowCol);
                                    if (objVal2 != null)
                                        sVal = objVal2.toString();
                                }
                            }
                            else
                            {
	                            String sWhere = String.format(" %s=%s", RefCol.getCode(),obj.GetColSqlValue(col));
	                            List<CBaseObject> lstObj2 = objRefMgr.GetList(sWhere);
	                            if (lstObj2.size() > 0)
	                            {
	                                CBaseObject obj2 = lstObj2.get(0);
	                                Object objVal2 = obj2.GetColValue(RefShowCol);
	                                if (objVal2 != null)
	                                    sVal = objVal2.toString();
	                            }
                            }
                        }
                    }
                    sRow += String.format("\"%s\":\"%s\",", col.getCode(), sVal);
                }
                else if (col.getColType() == ColumnType.datetime_type)
                {
                    String sVal = "";
                    Object objVal = obj.GetColValue(col);
                    if (objVal != null)
                    {
                    	Date dtime = (Date)objVal;
                    	SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss"); 

                        sVal = formatter.format(dtime); 
                    }

                    sRow += String.format("\"%s\":\"%s\",", col.getCode(), sVal);
                }
                else
                {
                    String sVal = "";
                    Object objVal = obj.GetColValue(col);
                    if (objVal != null)
                        sVal = objVal.toString();
                    //转义特殊字符
                    StringBuffer temp=new StringBuffer();
                    temp.append(sVal);
                    com.ErpCoreWeb.Common.Util.ConvertJsonSymbol(temp);
                    sVal=temp.toString();
                    sRow += String.format("\"%s\":\"%s\",", col.getCode(), sVal);
                }

            }
    		if (sRow.endsWith(","))
    			sRow = sRow.substring(0, sRow.length() - 1);
            sRow = "{" + sRow + "},";
            sData += sRow;
        }
		if (sData.endsWith(","))
			sData = sData.substring(0, sData.length() - 1);
        sData = "[" + sData + "]";
        String sJson = String.format("{\"Rows\":%s,\"Total\":\"%d\"}"
            , sData, totalCount);

        try {
			response.getWriter().print(sJson);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    void Delete()
    {
		try {
			String delid = request.getParameter("delid");
			if (Global.IsNullParameter(delid)) {
				response.getWriter().print("请选择记录！");
				return;
			}
			CBaseObjectMgr BaseObjectMgr = Global.GetCtx(
					this.getServletContext()).FindBaseObjectMgrCache(
					m_Table.getCode(), m_guidParentId);
			if (BaseObjectMgr == null) {
				BaseObjectMgr = new CBaseObjectMgr();
				BaseObjectMgr.TbCode = m_Table.getCode();
				BaseObjectMgr.Ctx = Global.GetCtx(this.getServletContext());
				if (BaseObjectMgr.Ctx.getMainDB().m_DbType == DatabaseType.MongoDb)
                {
                	BasicDBObject query = new BasicDBObject();  
                    query.put("_id", delid);
                    BaseObjectMgr.GetList(query, null, 0, 0);
                }
				else
				{
					String sWhere = String.format(" id='%s'", delid);
					BaseObjectMgr.GetList(sWhere);
				}
			}
			if (!BaseObjectMgr.Delete(Util.GetUUID(delid), true)) {
				response.getWriter().print("删除失败！");
				return;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    public String GetCompanyId()
    {
        CUser user = (CUser)request.getSession().getAttribute("User");
        return user.getB_Company_id().toString();
    }
}
